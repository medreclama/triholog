import flexslider from 'flexslider'; // eslint-disable-line
import fancybox from '@fancyapps/fancybox'; // eslint-disable-line
import consultationSwitch from '../blocks/consultation/consultation-aside/consultation-aside';
import mobileFilterMobile from '../blocks/catalog/catalog-filter-mobile/catalog-filter-mobile';
import mobileMenu from '../blocks/header/mobile-menu/mobile-menu';
import showHide from '../blocks/show-hide/show-hide';
import countdown from './external/jquery.countdown'; // eslint-disable-line
consultationSwitch();
mobileFilterMobile();
mobileMenu();
showHide();

$(document).ready(() => {
  $('.js-slider').flexslider({
    animation: 'slide',
    selector: '.js-slider__list > .js-slider__item',
    controlNav: false,
    touch: true,
    prevText: '',
    nextText: '',
    slideshow: false,
  });
  $.maskfn.definitions['~'] = '[+-]';
  $('body').delegate('input[type="tel"], input.phone', 'focus', () => {
    $(this).maskfn('+7 (999) 999-99-99');
  });
  $('.modal').fancybox();
});
