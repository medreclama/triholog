const mobileFilterExports = () => {
  const mobileFilterSelects = document.querySelectorAll('.mobile-filter-item__select');

  function mobileFilterActive() {
    const currentOptions = [];
    const activeLabel = this.parentNode.querySelector('.mobile-filter-item__label_active');
    const inactiveLabel = this.parentNode.querySelector('.mobile-filter-item__label_inactive');
    for (let j = 0; j < this.options.length; j += 1) {
      const option = this.options[j];
      if (option.selected) {
        currentOptions.push(option.innerText);
      }
    }
    if (currentOptions.length > 0) {
      activeLabel.innerText = currentOptions.join(', ');
      inactiveLabel.style.display = 'none';
    } else {
      activeLabel.innerText = '';
      inactiveLabel.style.display = 'block';
    }
  }
  for (let i = 0; i < mobileFilterSelects.length; i += 1) {
    mobileFilterSelects[i].addEventListener('focusout', mobileFilterActive);
    mobileFilterSelects[i].addEventListener('click', mobileFilterActive);
    mobileFilterSelects[i].addEventListener('change', mobileFilterActive);
  }

  document.addEventListener('DOMContentLoaded', () => {
    for (let i = 0; i < mobileFilterSelects.length; i += 1) {
      const currentOptions = [];
      const activeLabel = mobileFilterSelects[i].parentNode.querySelector('.mobile-filter-item__label_active');
      const inactiveLabel = mobileFilterSelects[i].parentNode.querySelector('.mobile-filter-item__label_inactive');
      for (let j = 0; j < mobileFilterSelects[i].options.length; j += 1) {
        const option = mobileFilterSelects[i].options[j];
        if (option.selected) {
          currentOptions.push(option.innerText);
        }
      }
      if (currentOptions.length > 0) {
        activeLabel.innerText = currentOptions.join(', ');
        inactiveLabel.style.display = 'none';
      } else {
        activeLabel.innerText = '';
        inactiveLabel.style.display = 'block';
      }
    }
  });
};

export default mobileFilterExports;
